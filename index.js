const person = {
  firstName: "Juan",
  middleName: "Dela",
  lastName: "Cruz",
};

const {
  firstName: givenName,
  middleName: maidenName,
  lastName: surName,
} = person;

console.log(givenName); // output is Juan

// using classes with constructor is same with object constructor